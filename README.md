# README #

Set up instructions

1. See server repository [server for this application](https://bitbucket.org/pao214/server) for details on how to run a server.
1. You would get an error: Configuration with name 'default' not found
1. In this case import volley project and add it as a dependency to main 'app' application.
(Since the method of importing module may depend upon android-studio version, you must figure out importing module for yourself).
1. Install SDK tools version 22.0.1 and Android-SDK platform-22 if not already present.
1. Change the base_url variable provided in LoginActivity.java class to your IP address.
1. Build the project for any errors and run the project.

### What is this repository for? ###

* Quick summary

* Version
Minimum SDK version required to run the project in a device is 15
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact