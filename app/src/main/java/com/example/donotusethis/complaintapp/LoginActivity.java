package com.example.donotusethis.complaintapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity{

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    public static LoginActivity instance;
    public static final String PREFS="MyPrefs";
    public static final String base_url = "http://192.168.202.1:8888/tut/ignite/";
    int backButtonCount = 0;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        instance = this;
        //Restore preferences
        SharedPreferences sharedPreferences=getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        String cookie=sharedPreferences.getString(CustomRequest.SESSION_COOKIE, null);
        if (cookie!=null)
            startActivity(new Intent(this, UserProfile.class));
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);

        openKeyboard(this, mEmailView);
    }

    @Override
    public void onBackPressed() {
        final Handler handler=new Handler(){
            @Override
            public void handleMessage(Message msg) {
                backButtonCount = 0;
            }
        };

        Runnable runnable = new Runnable() {
            public void run() {
                long futureTime=System.currentTimeMillis()+1000;
                while (System.currentTimeMillis()<futureTime){
                    synchronized (this){
                        try {
                            wait(futureTime-System.currentTimeMillis());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                handler.sendEmptyMessage(0);
            }
        };
        Thread thread= new Thread(runnable);
        thread.start();

        if(backButtonCount >= 1) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Press again", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }

    /**
     * sends focus and opens keyboard
     * @param activity
     * @param editText
     */
    public static void openKeyboard(Activity activity, View editText){

        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        ((InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(
                editText, InputMethodManager.SHOW_IMPLICIT);
    }

    private void populateAutoComplete() {

    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        String error = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }else if ((error= isPasswordValid(email))!=null){
            mPasswordView.setError(error);
            focusView=mPasswordView;
            cancel=true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }else if ((error= isEmailValid(email))!=null){
            mEmailView.setError(error);
            focusView=mEmailView;
            cancel=true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            sendRequest(email, password);
        }
    }

    public static String toAscii(String input){
        int len=input.length();
        StringBuilder builder=new StringBuilder();
        for(int i=0;i<len;i++){
            builder.append("%");
            builder.append(Integer.toHexString((int)input.charAt(i)));
        }
        return builder.toString();
    }

    /**
     * sends request
     * @param mEmail
     * @param mPassword
     */
    private void sendRequest(final String mEmail, final String mPassword) {

        String encrypt="";
        try {//jaffa
            byte[] rev_str = new StringBuffer(mPassword).reverse().toString().getBytes();
            MessageDigest passEncrypt= MessageDigest.getInstance("MD5");
            encrypt = new StringBuffer(passEncrypt.digest(rev_str).toString()).reverse().toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String url=base_url+"home/login?UserId="+mEmail+"&Password="+mPassword;
        RequestQueue requestQueue= Volley.newRequestQueue(instance);
        CustomRequest customRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            boolean success = jsonObject.getBoolean("success");
                            if (success) {
                                startActivity(new Intent(instance, UserProfile.class));
                            }else{
                                Toast.makeText(instance,
                                        jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                                mEmailView.requestFocus();
                            }
                        }catch(JSONException e){
                            Toast.makeText(instance, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message = error.getMessage();
                        if(message == null)
                            Toast.makeText(LoginActivity.this,
                                    "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        else
                        if (message.contains("java.net.Connect"))
                            Toast.makeText(instance,
                                    "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(instance, message, Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(customRequest);
    }

    /**
     * logic for email validation
     * @param email
     * @return
     */
    private String isEmailValid(String email) {

        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(email);
        if (m.find())
            return "No special characters allowed.";
        return null;
    }


    /**
     * logic for password validation
     * @param email
     * @return
     */
    private String isPasswordValid(String email) {

        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(email);
        if (m.find())
            return "No special characters allowed.";
        return null;
    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }
}

