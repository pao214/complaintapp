package com.example.donotusethis.complaintapp;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class UserProfile extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    UserProfile profile;
    TableRow HostelRow;
    TableRow RoomRow;
    TableRow WorkRow;
    int user_type;
    String user_id;
    int backButtonCount = 0;
    NavigationView navigationView;
    String first_name, last_name, room_no = "D-45";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        HostelRow=(TableRow)findViewById(R.id.HostelRow);
        RoomRow=(TableRow)findViewById(R.id.RoomNoRow);
        WorkRow=(TableRow)findViewById(R.id.WorkTypeRow);

        profile = this;
        displayUser();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        //jaffa
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    /**
     *
     */
    public void displayUser(){
        String url= LoginActivity.base_url+"users/user";
        RequestQueue requestQueue= Volley.newRequestQueue(profile);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject user=new JSONObject(response).getJSONObject("user");
                            int type = user.getInt("type");
                            JSONObject jsonObject = user.getJSONObject("info");
                            String special=jsonObject.getString("Special");
                            if (special.equals(String.valueOf(constants.normal))){
                                navigationView.getMenu().getItem(5).setVisible(false);
                            }
                            switch (type){
                                case constants.cons_student:
                                    String hostel_name = jsonObject.getString("HostelName");
                                    String room_no = jsonObject.getString("RoomNo");
                                    ((TextView)findViewById(R.id.hostel)).setText(
                                            hostel_name
                                    );
                                    ((TextView)findViewById(R.id.RoomNo)).setText(
                                            room_no
                                    );
                                    profile.room_no = room_no;
                                    WorkRow.setVisibility(View.GONE);
                                    break;

                                case constants.cons_warden:
                                    hostel_name = jsonObject.getString("HostelName");
                                    ((TextView)findViewById(R.id.hostel)).setText(
                                            hostel_name
                                    );
                                    RoomRow.setVisibility(View.GONE);
                                    WorkRow.setVisibility(View.GONE);
                                    break;

                                case constants.cons_staff:
                                    String work = jsonObject.getString("Type");
                                    ((TextView)findViewById(R.id.Work)).setText(
                                            work
                                    );
                                    HostelRow.setVisibility(View.GONE);
                                    RoomRow.setVisibility(View.GONE);
                                    break;
                                case constants.cons_faculty:
                                    HostelRow.setVisibility(View.GONE);
                                    RoomRow.setVisibility(View.GONE);
                                    WorkRow.setVisibility(View.GONE);

                            }
                            if(type == constants.cons_staff && jsonObject.get("Type").equals("director"))
                                type = constants.cons_director;
                            profile.user_type=type;

                            String user_id = jsonObject.getString("UserID");
                            profile.user_id = user_id;
                            String first_name = jsonObject.getString("FirstName");
                            String last_name = jsonObject.getString("LastName");
                            profile.first_name = first_name;
                            profile.last_name = last_name;
                            //jaffa-image
                            ((TextView)findViewById(R.id.UserProfile)).setText(
                                    "Welcome " + first_name + " !"
                            );
                            ((TextView)findViewById(R.id.UserId)).setText(
                                    user_id
                            );
                            ((TextView)findViewById(R.id.username)).setText(
                                    first_name+" "+last_name
                            );
                        }catch(JSONException e){
                            Toast.makeText(profile, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message=error.getMessage();
                        if (message==null)
                            Toast.makeText(profile,
                                    "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        else
                            if (message.contains("java.net.Connect"))
                                Toast.makeText(profile,
                                        "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                            else
                                Toast.makeText(profile, message, Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            final Handler handler=new Handler(){
                @Override
                public void handleMessage(Message msg) {
                    backButtonCount = 0;
                }
            };

            Runnable runnable = new Runnable() {
                public void run() {
                    long futureTime=System.currentTimeMillis()+1000;
                    while (System.currentTimeMillis()<futureTime){
                        synchronized (this){
                            try {
                                wait(futureTime-System.currentTimeMillis());
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    handler.sendEmptyMessage(0);
                }
            };
            Thread thread= new Thread(runnable);
            thread.start();

            if(backButtonCount >= 1) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                Toast.makeText(this, "Press again", Toast.LENGTH_SHORT).show();
                backButtonCount++;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            logOut(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {//jaffa-conditional
        int id = item.getItemId();

        if (id == R.id.nav_all) {
            Intent intent = new Intent(this, AllComplaints.class);
            intent.putExtra("type", user_type);
            intent.putExtra("user_id", user_id);
            startActivity(intent);
        } else if (id == R.id.nav_logout) {
            logOut(this);
        } else if (id == R.id.nav_individual) {
            startActivity(new Intent(this, LodgeIndividual.class));
        } else if (id == R.id.nav_hostel) {
            Intent intent = new Intent(this, LodgeHostel.class);
            intent.putExtra("hostel", true);
            startActivity(intent);
        } else if (id == R.id.nav_institute) {
            Intent intent = new Intent(this, LodgeHostel.class);
            intent.putExtra("hostel", false);
            startActivity(intent);
        } else if (id == R.id.nav_update) {
            Intent intent = new Intent(this, UpdateProfile.class);
            intent.putExtra("FirstName", first_name);
            intent.putExtra("LastName", last_name);
            intent.putExtra("RoomNo", room_no);
            intent.putExtra("type", user_type);
            startActivity(intent);

        }else if (id == R.id.nav_works){
            startActivity(new Intent(this, MyWorks.class));
        }else if (id == R.id.AddUser){
            startActivity(new Intent(this,AddUser.class));
        }else if (id == R.id.UpdateUser){
            startActivity(new Intent(this, UpdateUser.class));
        }else if (id == R.id.RemoveUser){
            startActivity(new Intent(this,RemoveUser.class));
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static void logOut(final Activity context){
        String url=LoginActivity.base_url+"home/logout";
        RequestQueue requestQueue= Volley.newRequestQueue(context);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        SharedPreferences.Editor editor=context.getSharedPreferences(LoginActivity.PREFS, 0).edit();
                        editor.remove(CustomRequest.SESSION_COOKIE);
                        editor.commit();
                        context.startActivity(new Intent(context, LoginActivity.class));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },context
        );
        requestQueue.add(stringRequest);
    }
}

