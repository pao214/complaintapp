package com.example.donotusethis.complaintapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class LodgeHostel extends AppCompatActivity {

    private LodgeHostel hostel;
    private boolean is_hostel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lodge_hostel);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        hostel = this;

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            is_hostel = bundle.getBoolean("hostel");
        }

        findViewById(R.id.Lodge).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                post_complaint();
            }
        });

    }

    public void post_complaint(){
        String title = ((EditText)findViewById(R.id.top)).getText().toString();
        String content = ((EditText)findViewById(R.id.description)).getText().toString();
        String gorl = "";
        if (is_hostel){
            gorl = "hostel/";
        }else{
            gorl = "institute/";
        }
        String url= LoginActivity.base_url+gorl+"complain?Title="+LoginActivity.toAscii(title)
                +"&Content="+LoginActivity.toAscii(content);
        RequestQueue requestQueue= Volley.newRequestQueue(hostel);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                Toast.makeText(hostel, "Your complaint has been successfully lodged", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(hostel,UserProfile.class));
                            }else{
                                Toast.makeText(hostel, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        }catch(JSONException e){
                            Toast.makeText(hostel, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message=error.getMessage();
                        if (message==null)
                            Toast.makeText(hostel,
                                    "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        else
                            if (message.contains("java.net.Connect"))
                                Toast.makeText(hostel,
                                        "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                            else
                                Toast.makeText(hostel, message, Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(stringRequest);
    }

}
