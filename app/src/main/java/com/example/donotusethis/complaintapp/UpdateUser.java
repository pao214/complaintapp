package com.example.donotusethis.complaintapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class UpdateUser extends AppCompatActivity {

    EditText userId, new_password, new_room;
    Spinner hostels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        hostels = (Spinner)findViewById(R.id.NewHostel);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, constants.hostels);
        hostels.setAdapter(adapter);

        userId = (EditText)findViewById(R.id.UserId);
        userId.setOnFocusChangeListener(
                new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus){
                            setUser();
                        }
                    }
                }
        );

        findViewById(R.id.updateuser).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updateUser();
                    }
                }
        );
    }

    public void setUser(){
        String url= LoginActivity.base_url+"users/getUser?UserId="+userId.getText().toString();
        final UpdateUser updateUser = this;
        RequestQueue requestQueue= Volley.newRequestQueue(updateUser);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject final_obj=new JSONObject(response);
                            if (final_obj.getBoolean("success")){
                                JSONObject user = final_obj.getJSONObject("user");
                                int type = user.getInt("type");
                                JSONObject jsonObject = user.getJSONObject("info");

                                if (jsonObject.getString("Special").equals(String.valueOf(constants.special))){
                                    Toast.makeText(updateUser, final_obj.getString("Message"), Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                switch (type){
                                    case constants.cons_student:
                                        findViewById(R.id.NewHostel).setVisibility(View.VISIBLE);
                                        findViewById(R.id.NewRoom).setVisibility(View.VISIBLE);
                                        hostels.setSelection(findPos(jsonObject.getString("HostelName")));
                                        new_room.setText(jsonObject.getString("RoomNo"));
                                        break;
                                    case constants.cons_faculty:
                                        findViewById(R.id.NewHostel).setVisibility(View.GONE);
                                        findViewById(R.id.NewRoom).setVisibility(View.GONE);
                                        break;
                                    case constants.cons_staff:
                                        findViewById(R.id.NewHostel).setVisibility(View.GONE);
                                        findViewById(R.id.NewRoom).setVisibility(View.GONE);
                                        break;
                                    case constants.cons_warden:
                                        findViewById(R.id.NewHostel).setVisibility(View.VISIBLE);
                                        findViewById(R.id.NewRoom).setVisibility(View.GONE);
                                        hostels.setSelection(findPos(jsonObject.getString("HostelName")));
                                        break;
                                    default:
                                        break;
                                }
                            }else{
                                Toast.makeText(updateUser, final_obj.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        }catch(JSONException e){
                            Toast.makeText(updateUser, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message=error.getMessage();
                        if (message==null)
                            Toast.makeText(updateUser,
                                    "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        else
                        if (message.contains("java.net.Connect"))
                            Toast.makeText(updateUser,
                                    "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(updateUser, message, Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(stringRequest);
    }

    public static int findPos(String hostel){
        for (int i=0; i<13; i++){
            if (constants.hostels[i].equals(hostel)){
                return i;
            }
        }
        return 0;
    }

    public void updateUser(){

        final UpdateUser updateUser = this;
        final String new_password=((EditText)findViewById(R.id.NewPassword)).getText().toString();
        final String new_room=((EditText)findViewById(R.id.NewRoom)).getText().toString();
        final String new_hostel = ((Spinner)findViewById(R.id.NewHostel)).getSelectedItem().toString();
        final String user_id = ((EditText)findViewById(R.id.UserId)).getText().toString();

        String url=LoginActivity.base_url+"users/update?UserId="
                +user_id+"&HostelName="+new_hostel+"&Password="+new_password+"&RoomNo="+new_room;
        RequestQueue requestQueue= Volley.newRequestQueue(updateUser);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                Toast.makeText(updateUser, "Succesfully updated " + user_id, Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(updateUser, UpdateUser.class));
                            }else{
                                Toast.makeText(updateUser, "Not possible", Toast.LENGTH_SHORT).show();
                            }
                        }catch(JSONException e){
                            Toast.makeText(updateUser, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message=error.getMessage();
                        if (message==null)
                            Toast.makeText(updateUser,
                                    "Message", Toast.LENGTH_SHORT).show();
                        else
                        if (message.contains("java.net.Connect"))
                            Toast.makeText(updateUser,
                                    "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(updateUser, message, Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(stringRequest);
    }
}
