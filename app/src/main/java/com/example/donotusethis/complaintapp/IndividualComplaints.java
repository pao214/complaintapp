package com.example.donotusethis.complaintapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by HARISH on 28-03-2016.
 */
public class IndividualComplaints extends Fragment {

    public IndividualComplaints() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.frg_individual_complaints, container, false);

        final AllComplaints activity=(AllComplaints)getActivity();

        String response=activity.individual_response;
        if (response==null)
            response="";
        try{

            final JSONObject jsonObject=new JSONObject(response);
            JSONArray pinned = jsonObject.getJSONArray("pinned");
            JSONArray unpinned = jsonObject.getJSONArray("unpinned");
            int pin_len = pinned.length();
            int unpin_len = unpinned.length();
            final JSONObject[] complaints=new JSONObject[pin_len+unpin_len];
            for (int i=0;i<pin_len;i++) {
                complaints[pin_len-i-1] = pinned.getJSONObject(i);
                complaints[pin_len-i-1].put("pin", constants.pinned);
            }
            for (int i=0; i<unpin_len; i++) {
                complaints[pin_len + unpin_len - i -1] = unpinned.getJSONObject(i);
                complaints[pin_len + unpin_len - i -1].put("pin", constants.unpinned);
            }
            ListAdapter listAdapter=new IndividualAdapter(activity, complaints);
            ListView listView=(ListView)view.findViewById(R.id.individualList);
            listView.setAdapter(listAdapter);
            listView.setOnItemClickListener(
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(getContext(), IndividualDescription.class);
                            try {
                                intent.putExtra("id", complaints[position].getString("id"));
                                intent.putExtra("sender", constants.sender);
                                intent.putExtra("pinit", complaints[position].getBoolean("pin"));
                            } catch (JSONException e) {

                            }
                            startActivity(intent);
                        }
                    }
            );

            view.findViewById(R.id.postMe).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(activity, LodgeIndividual.class));
                }
            });

        }catch (JSONException e){
            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return view;
    }
}
