package com.example.donotusethis.complaintapp;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by HARISH on 28-03-2016.
 */
public class IndividualAdapter extends ArrayAdapter<JSONObject> {

    public IndividualAdapter(Context context, JSONObject[] objects) {
        super(context, R.layout.frg_individual_adapter, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater myInflater= LayoutInflater.from(getContext());
        View myCustomview= myInflater.inflate(R.layout.frg_individual_adapter, parent, false);

        try{
            JSONObject singleItem= getItem(position);
            String updated_time = singleItem.getString("UpdatedTime");
            String title = singleItem.getString("Title");
            String content = singleItem.getString("Content");

            if (singleItem.getBoolean("pin")){
                myCustomview.setBackgroundColor(Color.parseColor("#80675623"));
            }

            if (content.length()>50)
                    content = content.substring(0, 50);
            ((TextView)myCustomview.findViewById(R.id.top)).setText(title);
            ((TextView)myCustomview.findViewById(R.id.createdTime)).setText(WorksAdapter.getDifference(updated_time));
            ((TextView)myCustomview.findViewById(R.id.content)).setText(content);//jaffa - receiver info
        }catch (JSONException e){
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return myCustomview;
    }
}
