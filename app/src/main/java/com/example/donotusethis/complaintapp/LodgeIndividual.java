package com.example.donotusethis.complaintapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class LodgeIndividual extends AppCompatActivity {

    private Spinner IndiType;
    private Button Lodge;
    private LodgeIndividual lodge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lodge_individual);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lodge = this;
        getTypes();
        findViewById(R.id.lodge_individual).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        post_complaint();
                    }
                }
        );
    }

    public void post_complaint(){
        String type = IndiType.getSelectedItem().toString();
        String title = ((EditText)findViewById(R.id.top)).getText().toString();
        String content = ((EditText)findViewById(R.id.description)).getText().toString();
        String url= LoginActivity.base_url+"individual/complain?type="+type+"&Title="+LoginActivity.toAscii(title)
                +"&Content="+LoginActivity.toAscii(content);
        RequestQueue requestQueue= Volley.newRequestQueue(lodge);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                startActivity(new Intent(lodge, Acknowledgement.class));
                            }else{
                                Toast.makeText(lodge, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        }catch(JSONException e){
                            Toast.makeText(lodge, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message=error.getMessage();
                        if (message==null)
                            Toast.makeText(lodge,
                                    "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        else
                        if (message.contains("java.net.Connect"))
                            Toast.makeText(lodge,
                                    "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(lodge, message, Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(stringRequest);
    }

    private void getTypes(){
        String url= LoginActivity.base_url+"individual/types";
        RequestQueue requestQueue= Volley.newRequestQueue(lodge);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONArray types=new JSONObject(response).getJSONArray("types");
                            String[] items = new String[types.length()];
                            for (int i=0; i<types.length(); i++){
                                items[i] = (String)types.get(i);
                            }

                            IndiType = (Spinner)findViewById(R.id.IndiType);
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(lodge, android.R.layout.simple_spinner_dropdown_item, items);
                            IndiType.setAdapter(adapter);
                        }catch(JSONException e){
                            Toast.makeText(lodge, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message=error.getMessage();
                        if (message==null)
                            Toast.makeText(lodge,
                                    "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        else
                        if (message.contains("java.net.Connect"))
                            Toast.makeText(lodge,
                                    "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(lodge, message, Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(stringRequest);
    }

}
