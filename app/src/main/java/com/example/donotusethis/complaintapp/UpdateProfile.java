package com.example.donotusethis.complaintapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class UpdateProfile extends AppCompatActivity {


    EditText firstName;
    EditText lastName;
    EditText oldPasswd;
    EditText newPasswd;
    EditText roomNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            String first_name = bundle.getString("FirstName");
            String last_name = bundle.getString("LastName");
            int type = bundle.getInt("type");
            String room_no = bundle.getString("RoomNo");
            firstName = (EditText)findViewById(R.id.NewFN);
            lastName = (EditText)findViewById(R.id.NewLN);
            roomNo = (EditText)findViewById(R.id.NewRoomNo);
            newPasswd = (EditText)findViewById(R.id.NewPass);
            firstName.setText(first_name);
            lastName.setText(last_name);
            roomNo.setText(room_no);
            if (type != constants.cons_student){
                roomNo.setVisibility(View.GONE);
            }
        }

        findViewById(R.id.mybutt).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updatePost();
                    }
                }
        );
    }

    public void updatePost(){

        if (newPasswd.getText().toString().equals("")){
            newPasswd.setText(oldPasswd.getText().toString());
        }

        String url=LoginActivity.base_url+"users/updateProfile?oldPassword="+oldPasswd.getText().toString()
                +"&FirstName"+firstName.getText().toString()+"&LastName="+lastName.getText().toString()+"&RoomNo="+
                roomNo.getText().toString()+"&newPassword"+newPasswd.getText().toString();
        final UpdateProfile context = this;
        RequestQueue requestQueue= Volley.newRequestQueue(context);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                startActivity(new Intent(context, UserProfile.class));
                            }else{
                                Toast.makeText(context, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e){
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },context
        );
        requestQueue.add(stringRequest);
    }

}
