package com.example.donotusethis.complaintapp;

/**
 * Created by HARISH on 27-03-2016.
 */
public class constants {

    public final static int cons_student = 1, cons_faculty = 2, cons_warden = 3, cons_staff = 4, cons_director=5,
            normal = 0, special = 1,
            individual=0, hostel=1, institute=2,
            novote = 0, upvote = 1, downvote = 2;

    public final static String[] hostels = {"KAILASH", "HIMADRI",
            "GIRNAR", "UDAIGIRI", "SATPURA",
            "ZANSKAR", "SHIVALIK", "VINDHYACHAL","KUMAON",
            "JWALAMUKHI", "ARAVALI", "KARAKORAM", "NILGIRI"};

    public final static String[] UserTypes={
            "Student","Faculty","Warden","Staff"
    };

    public final static String unresolved = "0", resolved = "1";

    public final static boolean sender = true, receiver = false, pinned = true, unpinned = false;
}