package com.example.donotusethis.complaintapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;

/**
 * Splash screen when app is opened and not logged in.
 * Implemented through threads
 */
public class SplashScreen extends AppCompatActivity {

    /**
     * Thread implementation of splash screen
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        final Handler handler=new Handler(){
            @Override
            public void handleMessage(Message msg) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        };

        Runnable runnable = new Runnable() {
            public void run() {
                long futureTime=System.currentTimeMillis()+2000;
                while (System.currentTimeMillis()<futureTime){
                    synchronized (this){
                        try {
                            wait(futureTime-System.currentTimeMillis());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                handler.sendEmptyMessage(0);
            }
        };
        Thread thread= new Thread(runnable);
        thread.start();
    }
}
