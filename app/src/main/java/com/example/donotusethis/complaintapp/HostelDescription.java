package com.example.donotusethis.complaintapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HostelDescription extends AppCompatActivity {

    RadioGroup radioGroup;
    int type;
    String id;
    boolean resolved = false;
    boolean pinned = false;
    boolean is_hostel = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hostel_description);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        radioGroup = (RadioGroup)findViewById(R.id.UpsDo);

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null) {
            final String id = bundle.getString("id");
            this.id = id;
            type = bundle.getInt("type");
            final int complaint = bundle.getInt("complaint");
            is_hostel = complaint == constants.hostel;
            String user_id = bundle.getString("user_id");
            pinned = bundle.getBoolean("pinit");

            if ((type == constants.cons_warden && complaint == constants.hostel)
                    || (type == constants.cons_director && complaint == constants.institute)) {
                //jaffa resolve button
            }

            loadAll(complaint, user_id);

            findViewById(R.id.Upbutton).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            vote(id, constants.upvote, complaint);
                        }
                    }
            );

            findViewById(R.id.DownButton).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            vote(id, constants.downvote, complaint);
                        }
                    }
            );

            findViewById(R.id.send).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            postComment(id,
                                        ((EditText)findViewById(R.id.post_comment)).getText().toString(),
                                        complaint == constants.hostel);
                        }
                    }
            );
        }
    }

    public void pin(){

        String gorl = "";
        if (is_hostel){
            gorl = "hostel/";
        }else{
            gorl = "institute/";
        }
        String url=LoginActivity.base_url+gorl+"unpin?id="+id;
        final HostelDescription context = this;
        RequestQueue requestQueue= Volley.newRequestQueue(context);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){

                            }else{
                                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e){
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },context
        );
        requestQueue.add(stringRequest);
    }

    public void resolve(){
        String gorl = "";
        if (is_hostel){
            gorl = "hostel/";
        }else{
            gorl = "institute/";
        }
        String url=LoginActivity.base_url+gorl+"resolve?id="+id;
        final HostelDescription description = this;
        RequestQueue requestQueue= Volley.newRequestQueue(description);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                startActivity(new Intent(description, AllComplaints.class));
                            }
                        }catch (JSONException e){
                            Toast.makeText(description, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(description, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },description
        );
        requestQueue.add(stringRequest);
    }

    public void postComment(final String id, final String content, final boolean is_hostel){
        String gorl = "";
        if (is_hostel){
            gorl = "hostel/";
        }else{
            gorl = "institute/";
        }
        String url=LoginActivity.base_url+gorl+"comment?id="+id+"&content="+LoginActivity.toAscii(content);
        final HostelDescription context = this;
        RequestQueue requestQueue= Volley.newRequestQueue(context);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                Intent intent = new Intent(context, HostelDescription.class);
                                intent.putExtra("id", id);
                                intent.putExtra("type", type);
                                if(is_hostel)
                                    intent.putExtra("complaint", constants.hostel);
                                else
                                    intent.putExtra("complaint", constants.institute);
                                startActivity(intent);
                            }else{
                                Toast.makeText(context, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e){
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },context
        );
        requestQueue.add(stringRequest);
    }

    public void vote (final String id, final int myVote, final int complaint){
        boolean is_hostel = complaint == constants.hostel;
        String gorl = "";
        if (is_hostel){
            gorl = "hostel/";
        }else{
            gorl = "institute/";
        }
        String url=LoginActivity.base_url+gorl+"vote?id="+id+"&upvote="+String.valueOf(myVote);
        final HostelDescription context = this;
        RequestQueue requestQueue= Volley.newRequestQueue(context);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                ((TextView)findViewById(R.id.Ups)).setText(String.valueOf(jsonObject.getInt("Upvotes")));
                                ((TextView)findViewById(R.id.Downs)).setText(String.valueOf(jsonObject.getInt("Downvotes")));
                            }
                        }catch (JSONException e){
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },context
        );
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_indi_desc, menu);
        if (pinned){
            menu.getItem(0).setTitle("UnPin");
        }else{
            menu.getItem(0).setTitle("Pin");
        }
        if (resolved){
            menu.getItem(1).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.Pin){
            pin();
            if (pinned){
                item.setTitle("UnPin");
            }else{
                item.setTitle("Pin");
            }
            return true;
        }else if (id == R.id.Resolve){
            resolve();
            return  true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void loadAll(int complaint, final String user_id){
        String gorl = "";
        final boolean is_hostel = complaint == constants.hostel;
        if (is_hostel){
            gorl = "hostel/";
        }else{
            gorl = "institute/";
        }
        String url=LoginActivity.base_url+gorl+"getcomplaint?id="+id;
        final HostelDescription description = this;
        RequestQueue requestQueue= Volley.newRequestQueue(description);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                JSONObject complaint = jsonObject.getJSONObject("complaint");
                                String title = complaint.getString("Title");
                                String content = complaint.getString("Content");
                                String created_time = complaint.getString("CreatedTime");

                                resolved = complaint.getString("Resolved") == constants.resolved;

                                int voted = complaint.getInt("Voted");
                                switch (voted){
                                    case constants.novote:
                                        break;
                                    case constants.upvote:
                                        radioGroup.check(R.id.Upbutton);
                                        break;
                                    case constants.downvote:
                                        radioGroup.check(R.id.DownButton);
                                        break;
                                    default:
                                        break;
                                }
                                ((TextView)findViewById(R.id.head)).setText(title);
                                ((TextView)findViewById(R.id.content)).setText(content);
                                ((TextView)findViewById(R.id.CreatedOn)).setText(created_time);
                                ((TextView)findViewById(R.id.Sender)).setText(
                                        complaint.getString("FirstName")+" "+complaint.getString("LastName")
                                );
                                ((TextView)findViewById(R.id.Ups)).setText(complaint.getString("Upvotes"));
                                ((TextView)findViewById(R.id.Downs)).setText(complaint.getString("Downvotes"));

                                JSONArray comments = complaint.getJSONArray("comments");
                                JSONObject[] adapts = new JSONObject[comments.length()];
                                for (int i=0; i<comments.length(); i++){
                                    JSONObject comment = comments.getJSONObject(i);
                                    if (comment.getString("UserId").equals(user_id)){
                                        comment.put("right", true);
                                    }else{
                                        comment.put("right", false);
                                    }
                                    adapts[i] = comment;
                                }

                                ListAdapter listAdapter=new HostelCommentView(description, adapts);
                                ListView listView=(ListView)findViewById(R.id.commentsList);
                                listView.setAdapter(listAdapter);

                            }else{
                                Toast.makeText(description, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        }catch(JSONException e){
                            Toast.makeText(description, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(description, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },description
        );
        requestQueue.add(stringRequest);
    }

}
