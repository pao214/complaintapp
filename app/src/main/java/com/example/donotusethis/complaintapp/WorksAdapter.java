package com.example.donotusethis.complaintapp;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by HARISH on 28-03-2016.
 */
public class WorksAdapter extends ArrayAdapter<JSONObject> {

    public WorksAdapter(Context context, JSONObject[] objects) {
        super(context, R.layout.frg_works_adapter, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater myInflater= LayoutInflater.from(getContext());
        View myCustomview= myInflater.inflate(R.layout.frg_works_adapter, parent, false);

        try{
            JSONObject singleItem= getItem(position);
            String updated_time = singleItem.getString("UpdatedTime");
            String title = singleItem.getString("Title");
            String content = singleItem.getString("Content");

            if (singleItem.getBoolean("pin")){
                myCustomview.setBackgroundColor(Color.parseColor("#80675623"));
            }

            if (content.length()>50)
                content = content.substring(0, 50);
            ((TextView)myCustomview.findViewById(R.id.head)).setText(title);
            ((TextView)myCustomview.findViewById(R.id.created_time)).setText(getDifference(updated_time));
            ((TextView)myCustomview.findViewById(R.id.content)).setText(content);
        }catch (JSONException e){
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return myCustomview;
    }

    public static String getDifference(String deadline){
        SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
        DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d1=null;
        Date d2=new Date();

        try{
            d1=format.parse(deadline);
            format.format(d2);
        }catch (ParseException e) {
            e.printStackTrace();
        }

        long diff=d2.getTime()-d1.getTime();
        String timeLeft="";

        long DiffSec=diff/1000%60;
        long DiffMin=diff/(60*1000)%60;
        long DiffHrs=diff/(60*60*1000)%24;
        long DiffDays=diff/(60*60*1000*24);
        if(DiffDays!=0)
            timeLeft=DiffDays+" days "+DiffHrs+":"+DiffMin+":"+DiffSec+timeLeft;
        else
            timeLeft=DiffHrs+":"+DiffMin+":"+DiffSec+timeLeft;
        return timeLeft+" ago";
    }
}
