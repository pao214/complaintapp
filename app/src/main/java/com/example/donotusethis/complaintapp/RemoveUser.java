package com.example.donotusethis.complaintapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class RemoveUser extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remove_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        findViewById(R.id.removeuser).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        removeUser();
                    }
                }
        );

    }

    public void removeUser(){

        final RemoveUser removeUser = this;
        final String user_id = ((EditText)findViewById(R.id.UserId)).getText().toString();
        String url=LoginActivity.base_url+"users/remove?UserId="+user_id;
        RequestQueue requestQueue= Volley.newRequestQueue(removeUser);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                Toast.makeText(removeUser, "Succesfully removed "+user_id, Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(removeUser, RemoveUser.class));
                            }else{
                                Toast.makeText(removeUser, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        }catch(JSONException e){
                            Toast.makeText(removeUser, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message=error.getMessage();
                        if (message==null)
                            Toast.makeText(removeUser,
                                    "Message", Toast.LENGTH_SHORT).show();
                        else
                        if (message.contains("java.net.Connect"))
                            Toast.makeText(removeUser,
                                    "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(removeUser, message, Toast.LENGTH_SHORT).show();
                    }
                },this
        );
        requestQueue.add(stringRequest);
    }
}
