package com.example.donotusethis.complaintapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IndividualDescription extends AppCompatActivity {

    String id;
    boolean resolved = false;
    boolean pinned = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_individual_description);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            final String id = bundle.getString("id");
            pinned = bundle.getBoolean("pinit");
            boolean sender = bundle.getBoolean("sender");
            if (!sender){
                findViewById(R.id.Resolve).setVisibility(View.GONE);
            }
            loadAll(sender, id);

            this.id = id;

            final EditText editText = (EditText)findViewById(R.id.post_comment);
            findViewById(R.id.send).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            postComment(id, editText.getText().toString());
                        }
                    }
            );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_indi_desc, menu);
        if (pinned){
            menu.getItem(0).setTitle("UnPin");
        }else{
            menu.getItem(0).setTitle("Pin");
        }
        if (resolved){
            menu.getItem(1).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.Pin){
            pin();
            return true;
        }else if (id == R.id.Resolve){
            resolve();
            return  true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void pin(){
        String url=LoginActivity.base_url+"individual/unpin?id="+id;
        final IndividualDescription context = this;
        RequestQueue requestQueue= Volley.newRequestQueue(context);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){

                            }else{
                                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e){
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },context
        );
        requestQueue.add(stringRequest);
    }

    public void postComment(final String id, String content){
        String url=LoginActivity.base_url+"individual/comment?id="+id+"&content="+LoginActivity.toAscii(content);
        final IndividualDescription indi = this;
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                Intent intent = new Intent(indi, IndividualDescription.class);
                                intent.putExtra("id", id);
                                intent.putExtra("sender", constants.receiver);
                                startActivity(intent);
                            }else{
                                Toast.makeText(indi, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e){
                            Toast.makeText(indi, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(indi, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },indi
        );
        requestQueue.add(stringRequest);
    }

    public void loadAll(final boolean sender, String id){
        String gorl = "";
        if (sender){
            gorl = "getcomplaint";
        }else{
            gorl = "getwork";
        }
        String url=LoginActivity.base_url+"individual/"+gorl+"?id="+id;
        final IndividualDescription description = this;
        RequestQueue requestQueue= Volley.newRequestQueue(description);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                JSONObject complaint = jsonObject.getJSONObject("complaint");
                                String right_id = "";
                                if (sender){
                                    right_id = complaint.getString("SenderID");
                                }else{
                                    right_id = complaint.getString("ReceiverID");
                                }
                                String resolved = complaint.getString("Resolved");
                                RadioButton radioButton = (RadioButton)findViewById(R.id.Resolve);
                                if (resolved.equals(constants.resolved)){
                                    radioButton.setChecked(true);
                                }else{
                                    radioButton.setChecked(false);
                                }
                                String title = complaint.getString("Title");
                                String content = complaint.getString("Content");
                                String created_time = complaint.getString("CreatedTime");
                                ((TextView)findViewById(R.id.head)).setText(title);
                                ((TextView)findViewById(R.id.content)).setText(content);
                                ((TextView)findViewById(R.id.CreatedOn)).setText(created_time);
                                JSONObject receiver;
                                if (sender){
                                    ((TextView)findViewById(R.id.sen_rec)).setText("Receiver");
                                    receiver = complaint.getJSONObject("receiver");
                                    ((TextView)findViewById(R.id.Sender)).setText(
                                            receiver.getString("FirstName")+" "+receiver.getString("LastName")
                                    );
                                }else{
                                    receiver = complaint.getJSONObject("sender");
                                    ((TextView)findViewById(R.id.Sender)).setText(
                                            receiver.getString("FirstName") + " " + receiver.getString("LastName")
                                    );
                                }
                                try{
                                    ((TextView)findViewById(R.id.Receiver)).setText(
                                            receiver.getString("numcomplaints")
                                    );
                                }catch(JSONException e){
                                    findViewById(R.id.fo).setVisibility(View.GONE);
                                }
                                JSONArray comments = complaint.getJSONArray("comments");
                                JSONObject[] adapts = new JSONObject[comments.length()];
                                for (int i=0; i<comments.length(); i++){
                                    JSONObject comment = comments.getJSONObject(i);
                                    if (comment.getString("UserId").equals(right_id)){
                                       comment.put("right", true);
                                    }else{
                                        comment.put("right", false);
                                    }
                                    adapts[i] = comment;
                                }
                                ListAdapter listAdapter=new CommentView(description, adapts);
                                ListView listView=(ListView)findViewById(R.id.commentsList);
                                listView.setAdapter(listAdapter);

                            }else{
                                Toast.makeText(description, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
                            }
                        }catch(JSONException e){
                            Toast.makeText(description, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(description, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },description
        );
        requestQueue.add(stringRequest);
    }

    public void resolve(){
        String url=LoginActivity.base_url+"individual/resolve?id="+id;
        final IndividualDescription description = this;
        RequestQueue requestQueue= Volley.newRequestQueue(description);
        CustomRequest stringRequest=new CustomRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getBoolean("success")){
                                startActivity(new Intent(description, AllComplaints.class));
                            }
                        }catch (JSONException e){
                            Toast.makeText(description, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(description, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                },description
        );
        requestQueue.add(stringRequest);
    }

    public static JSONObject[] reverse(JSONObject[] arr){
        JSONObject[] rev = new JSONObject[arr.length];
        int len = arr.length-1;
        for (int i=0; i<arr.length; i++){
            rev[len-i] = arr[i];
        }
        return rev;
    }
}
